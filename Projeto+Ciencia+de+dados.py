
# coding: utf-8

# In[135]:

import pandas as pd
    


# In[136]:

from pandas import DataFrame 


# In[137]:

import matplotlib.pyplot as plt
get_ipython().magic('matplotlib inline')


# In[138]:

#Exercicio 1 - Carregue a base de dados de tweets que está no formato csv. 
#Recomendamos utilizar a biblioteca Pandas isso vai facilitar muito sua análise.

twitter_dataset = pd.read_csv('twitter_airline.csv')


# In[139]:

#Exercicio 2 - Exiba a quantidade de tweets da base. Por exemplo,
#Essa base contém: 100 tweets.
#Utilizei o len para computar o tamanho total da base de dados 
#Twitter_dataset.index pega os dados do dataset criado 

len(twitter_dataset.index)


# In[140]:

#Exercicio 3 -Obtenha a frequência de ocorrência de cada empresa aérea (airline) e exiba essa informação. Por exemplo,
#TAM: 36
#Avianca: 78
#Azul: 15
#Foi um desafio e tanto essa, pois queria o valor apenas em relação a frequencia de cada empresa aerea e não no geral 
#Utilizei o value_counts que retorna a conta do valor desejado de maneira única em modo decrescente 
#Também coloquei a minha base de dados em um dataframe para visualização

df = DataFrame(twitter_dataset) 
df['airline'].value_counts()


# In[142]:

#Exercicio 4 - Apresente as 10 reclamações (negativereason)
#mais frequentes em relação ao sentimento negativo. Desconsidere os valores ausentes (NaN)
#Utilizei o metodo groupby para agrupar os dados da coluna negativereason e da coluna airlinesentiment
#em medida da relação pedida pelo exercicio
#usei o metodo count para contar a quantidade de sentimentos negativos em cada aspecto da base de dados 
#Os valores ausentes, foram desconsiderados em uma segunda analise, retirando as linhas com valores ausentes com
#dropna() 
contador = df.groupby(['negativereason'])[['airline_sentiment']].count()
contador


# In[143]:

#Segunda analise, sem as linhas que contém valores ausentes
df_no_missing = df.dropna()
contador2 = df_no_missing.groupby(['negativereason'])[['airline_sentiment']].count()
contador2


# In[144]:

#Exercicio 5 - Agora vamos trabalhar com gráficos. Nesse caso, recomendamos usar o matplotlib.
#a. Plote o histograma das empresas de aviação.
#b. Plote um gráfico pizza com a proporção de sentimentos da base de dados.
#c. Plote um gráfico de série temporal com a quantidade de tweets por dia. Desconsidere o horário.
#Eu importei a biblioteca matplotlib logo no começo do exercicio
#Esse sem via de duvidas foi a parte mais elaborada, pois tive que pesquisar os diferentes tipos de plotagem e como realizar
#cada um deles. 
#Para o exercicio 'a', visualizei a documentação do metodo plot() onde tinham varios parametros
#Alguns desses seria o tipo de grafico, titulo do grafico, ativar/desativar legendas, tamanho/altura do grafico
#eixos da figura associados como objetos do matplotlib para dividir em subgraficos (neste caso), entre muitos outros...
#juntamente a essas opções, usei o value_counts pra pegar o valor único da coluna desejada 

fig, ax = plt.subplots()
df['airline'].value_counts().plot(ax=ax, kind='hist',title='Linhas aereas',legend=True,figsize=(10,10))


# In[145]:

#Para o segundo exercicio o 'b', utilizei o mesmo modelo anterior, porém com o grafico em formato de pizza
#também com a proporção dos sentimentos da base de dados (airline_sentiments) 

fig, ax = plt.subplots()
df['airline_sentiment'].value_counts().plot(ax=ax, kind='pie',title='Sentimento',legend=True,figsize=(10,10))


# In[134]:

#Para o terceiro exercicio o 'c', utilizei o metodo grupby agrupando apenas pelo dia e descartando o restante
#foi uma tarefa complicada encontrar o metodo certo pela internet e algumas complicações em outras tentativas que 
#não seriam o resultado desejado - a coluna e o index continham o mesmo nome 'tweet_created' 


testando = df.groupby(df.tweet_created.index.day).count()
testando.plot(title='Dia',legend=True,figsize=(10,10))


# In[ ]:

#Essa base de dados foi um grande aprendizado em meio de tantas pequisas gastas no fim de semana 
#Com certeza, o maior desafio foi me motivar a aprender python e utilizar suas bibliotecas, esse conhecimento vai me
#ajudar futurumente (nesta vaga ou quem sabe num futuro distante) :)
#Python é uma linguagem fluida e fácil de usar, certamente é algo que ficará estagnado como conhecimento obrigatoriio 
#e passará conhecidas como java e C 
#Agradeço pela oportunidade de aprendizado e pela atenção do Rafael 
#Outras informações e todos os comentários digitados aqui estão nos pdf junto ao repositorio deste arquivo

